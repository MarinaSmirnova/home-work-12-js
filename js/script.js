document.addEventListener('keydown', function (event) {
    if ((event.code == "KeyS") || (event.code == "KeyE") || (event.code == "KeyO") || (event.code == "KeyN") || (event.code == "KeyL") || (event.code == "KeyZ")) {
        console.log(event.code[3]);
        paintButton(event.code[3]);
    } else if (event.code == "Enter") {
        console.log(event.code);
        paintButton(event.code);
    }

    function paintButton(code) {
        const buttons = document.querySelectorAll(".btn");
        for (const button of buttons) {
            button.classList.remove("blue");
            if (button.innerHTML == code) {
                button.classList.add("blue");
                
            }
        }
    }
});

